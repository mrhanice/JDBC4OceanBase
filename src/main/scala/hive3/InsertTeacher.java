package hive3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

public class InsertTeacher {
    public static String getRandomName(int length) {
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i< length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public static String getRandomCourse() {
        String couress[] = {"Chinese","Math","English","Physical","Chemisty","Biology","History","Geography","Politics","CS"};
        int id =  new Random().nextInt(100) % couress.length;
        return couress[id];
    }

    public static void main(String[] args) {
        String driverClassName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://ecnu05:3306/mytest?useSSL=false&allowPublicKeyRetrieval=true";
        String username = "root";	//数据库用户名
        String password = "123456";	//数据库连接密码
        Connection con = null;		//连接
        PreparedStatement pstmt = null;	//使用预编译语句
        ResultSet rs = null;	//获取的结果集
        try {
            Class.forName(driverClassName); //执行驱动
            con = DriverManager.getConnection(url, username, password); //获取连接
//            String sql = "select * from student";

            String sql = "INSERT INTO teacher VALUES(?,?,?,?)"; //设置的预编译语句格式
            pstmt = con.prepareStatement(sql);
            for(int i = 10; i < 100000; i++) {
                pstmt.setInt(1,i);
                pstmt.setString(2,getRandomName(3));
                pstmt.setInt(3,new Random().nextInt(100) % 10 + 20);
                pstmt.setString(4,getRandomCourse());
                int anscode = pstmt.executeUpdate();
                System.out.println("anscode = " + anscode);
            }
//            rs = pstmt.executeQuery();
//            while(rs.next()) {
//                int id = rs.getInt(1);
//                String name = rs.getString(2);
//                int age = rs.getInt(3);
//                System.out.println(id + " " + name + " " + age);
//            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
            //关闭资源,倒关
            try {
                if(rs != null) rs.close();
                if(pstmt != null) pstmt.close();
                if(con != null) con.close();  //必须要关
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
