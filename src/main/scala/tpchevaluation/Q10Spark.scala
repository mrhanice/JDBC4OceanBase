package tpchevaluation

import org.apache.spark.sql.SparkSession

object Q10Spark {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("TestQ1Spark").getOrCreate()
    //    val spark: SparkSession = SparkSession.builder().appName("TestQ1Spark").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0);//String database = args[0];
//    val table = args(1); //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(1).toInt; //int numPartitions = Integer.parseInt(args[5]);
    println("database = " + database)
//    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val one = System.currentTimeMillis()
    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "lineitem")
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option("partitionsNew",true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    val customer = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "customer")
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option("partitionsNew",true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    customer.createOrReplaceTempView("customer")

    val orders = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "orders")
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option("partitionsNew",true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    orders.createOrReplaceTempView("orders")

    val nation = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "nation")
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .load()
    nation.createOrReplaceTempView("nation")

    val q1 = spark.sql("""SELECT
                         |       c_custkey,
                         |       c_name,
                         |       SUM(l_extendedprice * (1 - l_discount)) AS revenue,
                         |       c_acctbal,
                         |       n_name,
                         |       c_address,
                         |       c_phone,
                         |       c_comment
                         |FROM
                         |    customer,
                         |    orders,
                         |    lineitem,
                         |    NATION
                         |WHERE
                         |  c_custkey = o_custkey
                         |  AND l_orderkey = o_orderkey
                         |  AND l_returnflag = 'R'
                         |  AND c_nationkey = n_nationkey
                         |GROUP BY
                         |    c_custkey,
                         |    c_name,
                         |    c_acctbal,
                         |    c_phone,
                         |    n_name,
                         |    c_address,
                         |    c_comment
                         |ORDER BY
                         |    revenue DESC""".stripMargin)
    q1.show()
    val two = System.currentTimeMillis()
    println("Execution Time = " + (two - one)/1000.0)
    //    val cnt: Long = q1.count()
    //    println("cnt = " + cnt)
  }
}
