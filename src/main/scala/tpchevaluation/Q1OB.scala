package tpchevaluation

import org.apache.spark.sql.SparkSession

object Q1OB {
  def main(args: Array[String]): Unit = {
//    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("TestQ1OB").getOrCreate()
    val spark: SparkSession = SparkSession.builder().appName("TestQ1OB").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0);//String database = args[0];
    val table = args(1); //String table = args[1];
//    val partitionColumn = args(2); //String partitionColumn = args[2];
//    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
//    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
//    val numPartitions = args(5).toInt; //int numPartitions = Integer.parseInt(args[5]);
    println("database = " + database)
    println("table = " + table)
//    println("numPartitions = " + numPartitions)
    val one = System.currentTimeMillis()
    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "tpchq1")
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .load()
    lineitem.createOrReplaceTempView("tpchq1")

    val q1 = spark.sql("""select * from tpchq1""".stripMargin)
    q1.show()
    val two = System.currentTimeMillis()
    println("Execution Time = " + (two - one) / 1000.0)
  }
}
