package tpchevaluation

import org.apache.spark.sql.SparkSession

object Q1Spark {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("TestQ1Spark").getOrCreate()
//    val spark: SparkSession = SparkSession.builder().appName("TestQ1Spark").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0);//String database = args[0];
    val table = args(1); //String table = args[1];
//    val partitionColumn = args(2); //String partitionColumn = args[2];
//    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
//    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(2).toInt; //int numPartitions = Integer.parseInt(args[5]);
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val one = System.currentTimeMillis()
    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", table)
//      .option("partitionColumn", partitionColumn)
//      .option("lowerBound", lowerBound)
//      .option("upperBound", upperBound)
      .option("partitionsNew",true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    val q1 = spark.sql("""SELECT
        |        l_returnflag,
        |        l_linestatus,
        |        SUM(l_quantity) AS sum_qty,
        |        SUM(l_extendedprice) AS sum_base_price,
        |        SUM(l_extendedprice * (1 - l_discount)) AS sum_disc_price,
        |        SUM(l_extendedprice * (1 - l_discount) * (1 + l_tax)) AS sum_charge,
        |        AVG(l_quantity) AS avg_qty,
        |        AVG(l_extendedprice) AS avg_price,
        |        AVG(l_discount) AS avg_disc,
        |        COUNT(*) AS count_order
        |FROM
        |        lineitem
        |WHERE
        |        l_shipdate <= DATE '1998-12-01' - INTERVAL '90' DAY
        |GROUP BY
        |        l_returnflag,
        |        l_linestatus
        |ORDER BY
        |        l_returnflag,
        |        l_linestatus
        |""".stripMargin)
    q1.show()
    val two = System.currentTimeMillis()
    println("Execution Time = " + (two - one)/1000.0)
//    val cnt: Long = q1.count()
//    println("cnt = " + cnt)
  }
}
