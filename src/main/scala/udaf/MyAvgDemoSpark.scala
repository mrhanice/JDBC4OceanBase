package udaf

import org.apache.spark.sql.SparkSession

object MyAvgDemoSpark {
  def main(args: Array[String]): Unit = {
//    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("MyAvgDemoSpark").getOrCreate()
    val spark: SparkSession = SparkSession.builder().appName("MyAvgDemoSpark").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0)//String database = args[0];
    val table = args(1) //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(2).toInt //int numPartitions = Integer.parseInt(args[5]);
    val partitionType = args(3).toString
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val one = System.currentTimeMillis()
    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", table)
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option(partitionType,true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

      val part = spark.read
        .format("jdbc")
        .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
        .option("dbtable", "part")
        //      .option("partitionColumn", partitionColumn)
        //      .option("lowerBound", lowerBound)
        //      .option("upperBound", upperBound)
        .option(partitionType,true)
        .option("numPartitions", numPartitions)
        .option("user", "root@test_mrhanice")
        .option("password", "123456")
        .option("driver", "com.mysql.jdbc.Driver")
        .option("fetchsize", 100)
        .loadAutoParallel()
    part.createOrReplaceTempView("part")

    spark.udf.register("myAvg", new MyAvg)

//    val q1 = spark.sql("select l_quantity, myAvg(l_suppkey) from lineitem group by l_quantity")
//    val q1 = spark.sql("""select l_quantity from lineitem group by l_quantity""".stripMargin)
    val q1 = spark.sql(
    """SELECT
      |        100.00 * SUM(CASE
      |                WHEN p_type LIKE 'PROMO%'
      |                        THEN l_extendedprice * (1 - l_discount)
      |                ELSE 0
      |        END) / SUM(l_extendedprice * (1 - l_discount)) AS promo_revenue
      |FROM
      |        lineitem,
      |        part
      |WHERE
      |        l_partkey = p_partkey
      |        AND l_shipdate >= DATE '1995-09-01'
      |        AND l_shipdate < DATE '1995-09-01' + INTERVAL '1' MONTH
      """.stripMargin)
//    q1.explain(true)
    q1.show()
    val two = System.currentTimeMillis()
    println("Execution Time = " + (two - one)/1000.0)
  }
}
