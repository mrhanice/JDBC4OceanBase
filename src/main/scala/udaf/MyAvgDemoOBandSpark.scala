package udaf

import org.apache.spark.sql.SparkSession

object MyAvgDemoOBandSpark {
  def main(args: Array[String]): Unit = {
//    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("MyAvgDemoOBandSpark").getOrCreate()
    val spark: SparkSession = SparkSession.builder().appName("MyAvgDemoOBandSpark").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val database = args(0)//String database = args[0];
    val table = args(1) //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(2).toInt //int numPartitions = Integer.parseInt(args[5]);
    val partitionType = args(3).toString
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val one = System.currentTimeMillis()

    val tpchq1 = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
//      .option(partitionType,true)
//      .option("numPartitions", numPartitions)
      .option("dbtable", "tpchq14")
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .load()
    tpchq1.createOrReplaceTempView("tpchq14")
    val q1 = spark.sql("select * from tpchq14")
    q1.show()
    val two = System.currentTimeMillis()
    println("The execution time of SQL1 on OB is " + (two - one)/1000)

    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", table)
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option(partitionType,true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")
//
    val part = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "part")
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option(partitionType,true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    part.createOrReplaceTempView("part")

    spark.udf.register("myAvg", new MyAvg)
//
    val q2 = spark.sql("""SELECT
                         |      myAvg(l_suppkey)
                         |FROM
                         |        lineitem,
                         |        part
                         |WHERE
                         |        l_partkey = p_partkey
                         |        AND l_shipdate >= DATE '1995-09-01'
                         |        AND l_shipdate < DATE '1995-09-01' + INTERVAL '1' MONTH""".stripMargin)
//    q2.explain(true)
    q2.show()
    val three = System.currentTimeMillis()
    println("Total Execution Time = " + Math.max((three - two)/1000.0,(two - one)/1000.0))
  }
}
