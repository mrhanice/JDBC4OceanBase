package ml

import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession

object K_Means4OB {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("jdbc4OBScala").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val database = "tpch_1g_part"; //String database = args[0];
    val table = "lineitem"; //String table = args[1];
    val partitionColumn = "l_orderkey"; //String partitionColumn = args[2];
    val lowerBound = 1; //Long.parseLong(args[3]);
    val upperBound = 6000000; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = 3; //int numPartitions = Integer.parseInt(args[5]);

    val one = System.currentTimeMillis()
    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.23:2883/" + database)
      .option("dbtable", table)
      .option("partitions", true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")
    val frame = spark.sql("select l_orderkey,l_partkey from lineitem")
    frame.cache()
    frame.count()
    val two = System.currentTimeMillis()
    println("The time of loading data from OceanBase to Spark is " + (two - one) * 1.0 / 1000)

    val assembler = new VectorAssembler().setInputCols(Array("l_orderkey", "l_partkey")).setOutputCol("features")
    val frame1 = assembler.setHandleInvalid("skip").transform(frame)
    //    frame1.cache()
    frame1.count()
    val three = System.currentTimeMillis()
    println("The time of transforming Row to vector is " + (three - two) * 1.0 / 1000)

    val kmeans = new KMeans().setK(2).setSeed(1L)
    val model = kmeans.fit(frame1)
    val four = System.currentTimeMillis()
    println("The time of k-means is " + (four - three) * 1.0 / 1000)
    println("Cluster Centers: ")
    model.clusterCenters.foreach(println)

    //    val predictions = model.transform(frame1)
    //    predictions.collect().foreach(println)
  }
}
