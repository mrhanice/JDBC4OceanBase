package ml

import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}

object LinearRegression4OB {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().appName("jdbc4OBScala").getOrCreate()
    //    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("jdbc4OBScala").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    args.foreach(println)
    println(args.length)

    val database = args(0) //String database = args[0];
    val table = args(1) //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(2).toInt //int numPartitions = Integer.parseInt(args[5]);
    val partititionType = args(3).toString
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val lineitem: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      //      .option("url", "jdbc:mysql://10.11.1.196:2883/" + database + "?useServerPrepStmts=true")
      .option("dbtable", table)
      //      .option("partitionsOld", true)
      .option(partititionType, true)
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    val assembler: VectorAssembler = new VectorAssembler().setInputCols(Array("l_partkey")).setOutputCol("features")
    //    val frame = spark.sql("select l_orderkey,l_partkey from lineitem where l_orderkey >= 1 and l_orderkey <= 300")
    //    frame.cache()
    //    val frame1 = assembler.setHandleInvalid("skip").transform(frame).select("l_orderkey", "features").toDF("label", "features")
    //
    //    val regression = new LinearRegression()
    //      .setMaxIter(10)
    //      .setRegParam(0.3)
    //      .setElasticNetParam(0.8)
    //
    //    val pipeline = new Pipeline().setStages(Array(regression))
    //    val model = pipeline.fit(frame1)
    //    model.write.overwrite().save("hdfs://ecnu05:9000/spark-mllib-dir/linearRegression4OB")
    //    val model = regression.fit(frame1)
    val one = System.currentTimeMillis()
    //    val testdata = spark.sql("select l_orderkey,l_partkey from lineitem where l_orderkey > 300 and l_orderkey <= 500")
    val testdata = spark.sql("select l_partkey from lineitem")
    //    testdata.cache()
    //    testdata.count()

    val two = System.currentTimeMillis()
    println("The time of loading data from OceanBase to Spark is " + (two - one) * 1.0 / 1000)

    val frame2 = assembler.setHandleInvalid("skip").transform(testdata)
    //    frame2.count()
    val three = System.currentTimeMillis()
    println("The time of transforming Row to vector is " + (three - two) * 1.0 / 1000)

    //    val model = PipelineModel.read.load("hdfs://ecnu05:9000/spark-mllib-dir/linearRegression4OB")
    val model = PipelineModel.read.load("hdfs://10.11.6.126:9105/hanbing/OBdir/MLdir/spark-mllib-dir/linearRegression4OB")
    val frame3 = model.transform(frame2)
    val cnt = frame3.count()
    println("cnt = " + cnt)
    val four = System.currentTimeMillis()
    println("The time of prediction is " + (four - one) * 1.0 / 1000)
    //    frame3.collect().foreach(println)
  }
}
