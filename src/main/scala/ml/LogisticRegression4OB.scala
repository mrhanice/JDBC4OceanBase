package ml

import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}

object LogisticRegression4OB {
  def main(args: Array[String]): Unit = {
    //    val spark = SparkSession.builder().appName("jdbc4OBScala").getOrCreate()
    val spark: SparkSession = SparkSession.builder()
      .master("local[*]")
      .appName("ThesisQ1")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

//    val database = args(0); //String database = args[0];
    val url = "jdbc:mysql://10.11.6.23:2883/"
//    val url = "jdbc:mysql://172.23.123.27:3306/"
    val database = "tpch_1g_part"; //String database = args[0];
    val table = "lineitem"; //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
//    val numPartitions = args(2).toInt; //int numPartitions = Integer.parseInt(args[5]);
    val numPartitions = 3; //int numPartitions = Integer.parseInt(args[5]);
    val partititionType = "partitionsOld"
//    val partititionType = args(3).toString
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val lineitem = spark.read
      .format("jdbc")
//      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("url", url + database)
      .option("dbtable", table)
      .option(partititionType, true)
      //      .option("partitionColumn", partitionColumn)
      //      .option("lowerBound", lowerBound)
      //      .option("upperBound", upperBound)
      .option("numPartitions", numPartitions)
//      .option("user", "root@test_mrhanice")
      .option("user", "root@eva")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
//      .load()
    lineitem.createOrReplaceTempView("lineitem")

    val frame = spark.sql(
      """select
        |    l_orderkey, l_partkey, l_suppkey, l_linenumber, l_quantity, l_extendedprice, l_discount, l_tax
        |from
        |    lineitem
        |where
        |    l_orderkey >= 1 and l_orderkey <= 100000
        |""".stripMargin)

    val assembler = new VectorAssembler().setInputCols(Array("l_orderkey", "l_partkey", "l_suppkey", "l_quantity", "l_extendedprice", "l_discount", "l_tax")).setOutputCol("features")
    val frame1 = assembler.setHandleInvalid("skip").transform(frame).select("l_linenumber", "features").toDF("label", "features")
//    frame1.explain(true)
//    frame1.show()

    //    val lr = new LogisticRegression()
    //      .setMaxIter(10)
    //      .setRegParam(0.3)
    //      .setElasticNetParam(0.8)

    //    val pipeline = new Pipeline().setStages(Array(lr))
    //    val model = pipeline.fit(frame1)
    //    model.write.overwrite().save("hdfs://ecnu05:9000/spark-mllib-dir/LogisticRegression4OB")
    //    model.write.overwrite().save("hdfs://10.11.6.126:9105/hanbing/OBdir/MLdir/spark-mllib-dir/LogisticRegression4OB")

//    val one = System.currentTimeMillis()
//    val testdata = spark.sql("select l_orderkey, l_partkey, l_suppkey, l_quantity, l_extendedprice, l_discount, l_tax from lineitem")
//
//    val frame2: DataFrame = assembler.setHandleInvalid("skip").transform(testdata).select("features")
//
    val model = PipelineModel.read.load("hdfs://10.11.6.126:9105/hanbing/OBdir/MLdir/spark-mllib-dir/LogisticRegression4OB")
    val frame2: DataFrame = model.transform(frame1)
    frame2.show()
//    frame2.explain(true)
//    val cnt = frame3.count()
//    println("cnt = " + cnt)
//    val four = System.currentTimeMillis()
//    println("The time of prediction is " + (four - one) * 1.0 / 1000)
  }
}
