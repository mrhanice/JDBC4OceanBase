package ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}

object LogisticRegressionTrain {
  def main(args: Array[String]): Unit = {
    //    val spark = SparkSession.builder().appName("jdbc4OBScala").getOrCreate()
    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("jdbc4OBScala").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val database = args(0); //String database = args[0];
    val table = args(1); //String table = args[1];
    //    val partitionColumn = args(2); //String partitionColumn = args[2];
    //    val lowerBound = args(3).toLong; //Long.parseLong(args[3]);
    //    val upperBound = args(4).toLong; //long upperBound = Long.parseLong(args[4]);
    val numPartitions = args(2).toInt; //int numPartitions = Integer.parseInt(args[5]);
    println("database = " + database)
    println("table = " + table)
    println("numPartitions = " + numPartitions)

    val lineitem = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.23:2883/" + database)
      .option("dbtable", table)
      .option("partitionsNew", true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 100)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")
    val frame = spark.sql("select l_orderkey, l_partkey, l_suppkey, l_linenumber, l_quantity, l_extendedprice, l_discount, l_tax from lineitem where l_orderkey >= 1 and l_orderkey <= 100000")

    val assembler = new VectorAssembler().setInputCols(Array("l_orderkey", "l_partkey", "l_suppkey", "l_quantity", "l_extendedprice", "l_discount", "l_tax")).setOutputCol("features")
    val frame1 = assembler.setHandleInvalid("skip").transform(frame).select("l_linenumber", "features").toDF("label", "features")

    val lr = new LogisticRegression()
      .setMaxIter(10)
      .setRegParam(0.3)
      .setElasticNetParam(0.8)
    //
    val pipeline = new Pipeline().setStages(Array(lr))
    val model = pipeline.fit(frame1)
    //    model.write.overwrite().save("hdfs://ecnu05:9000/spark-mllib-dir/LogisticRegression4OB")
    model.write.overwrite().save("hdfs://10.11.6.126:9105/hanbing/OBdir/MLdir/spark-mllib-dir/LogisticRegression4OB")
    //
    //    val one = System.currentTimeMillis()
    val testdata = spark.sql("select l_orderkey, l_partkey, l_suppkey, l_quantity, l_extendedprice, l_discount, l_tax from lineitem where l_orderkey > 100000 and l_orderkey <= 100100")
    val frame2: DataFrame = assembler.setHandleInvalid("skip").transform(testdata).select("features")
    //    val model = PipelineModel.read.load("hdfs://ecnu05:9000/spark-mllib-dir/LogisticRegression4OB")
    val frame3: DataFrame = model.transform(frame2)
    frame3.show()
    //    println("cnt = " + frame3.count())
    //    frame3.collect().foreach(println)
    //    val testdata = spark.sql("select l_partkey from lineitem")
    //    testdata.cache()
    //    testdata.count()
    //
    //    val two = System.currentTimeMillis()
    //    println("The time of loading data from OceanBase to Spark is " + (two - one)*1.0/1000)

    //    val frame2 = assembler.setHandleInvalid("skip").transform(testdata)
    //    frame2.count()
    //    val three = System.currentTimeMillis()
    //    println("The time of transforming Row to vector is " + (three - two)*1.0/1000)

    //    val model = PipelineModel.read.load("hdfs://ecnu05:9000/spark-mllib-dir/LogisticRegression4OB")
    //    val model = PipelineModel.read.load("hdfs://10.11.6.126:9105/hanbing/OBdir/MLdir/spark-mllib-dir/LogisticRegression4OB")
    //    val frame3 = model.transform(frame2)
    //    val cnt = frame3.count()
    //    println("cnt = " + cnt)
    //    val four = System.currentTimeMillis()
    //    println("The time of prediction is " + (four - three)*1.0/1000)
    //    frame3.collect().foreach(println)
  }
}
