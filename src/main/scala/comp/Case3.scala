package comp

import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import udaf.MyAvg

object Case3 {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.
      builder()
      .master("local[*]")
      .appName("Case3")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0)//String database = args[0];
    val numPartitions = args(1).toInt //int numPartitions = Integer.parseInt(args[5]);
    val lowerBound1 = args(2).toInt
    val upperBound1 = args(3).toInt
    val lowerBound2 = args(4).toInt
    val upperBound2 = args(5).toInt
    val lowerBound3 = args(6).toInt
    val upperBound3 = args(7).toInt

    val one = System.currentTimeMillis()
    val lineitem: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "lineitem")
      .option("partitionColumn","l_orderkey")
      .option("lowerBound",lowerBound1)
      .option("upperBound",upperBound1)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    val orders: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "orders")
      .option("partitionColumn","o_orderkey")
      .option("lowerBound",lowerBound2)
      .option("upperBound",upperBound2)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    orders.createOrReplaceTempView("orders")

    val customer: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "customer")
      .option("partitionColumn","c_custkey")
      .option("lowerBound",lowerBound3)
      .option("upperBound",upperBound3)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    customer.createOrReplaceTempView("customer")

    val nation: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "nation")
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    nation.createOrReplaceTempView("nation")

    spark.udf.register("myUdf", new MyAvg)
    val input: DataFrame = spark.sql(
      """select
        |        c_custkey,
        |        c_name,
        |        sum(l_extendedprice * (1 - l_discount)) as revenue,
        |        myUdf(c_custkey),
        |        c_acctbal,
        |        n_name,
        |        c_address,
        |        c_phone,
        |        c_comment
        |from
        |        customer,
        |        orders,
        |        nation,
        |        lineitem
        |where
        |        c_custkey = o_custkey
        |        and l_orderkey = o_orderkey
        |        and o_orderdate >= date '1993-10-01'
        |        and o_orderdate < date '1993-10-01' + interval '3' month
        |        and l_returnflag = 'R'
        |        and c_nationkey = n_nationkey
        |group by
        |        c_custkey,
        |        c_name,
        |        c_acctbal,
        |        c_phone,
        |        n_name,
        |        c_address,
        |        c_comment
        |order by
        |        revenue desc""".stripMargin)
    input.explain(true)
//    val condition = true
//    var result: Dataset[(String,Double)] = null
//    if (condition) {
//      result = input.map(x => {
//        Tuple2(x.getString(1),x.getDouble(3))
//      })
//    } else {
//      result = input.map(x => Tuple2(x.getString(1), x.getDouble(2)))
//    }
//    result.show()
//    val two = System.currentTimeMillis()
//    println("Total execution time is " + 1.0*(two - one)/1000)
  }
}
