package comp

import org.apache.spark.sql.catalog.Catalog
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import udaf.MyAvg

object Case1 {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.
      builder()
      .master("local[*]")
      .appName("Case1")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val database = args(0) //String database = args[0];
    val numPartitions = args(1).toInt //int numPartitions = Integer.parseInt(args[5]);
    val lowerBound = args(2).toInt
    val upperBound = args(3).toInt

    val one = System.currentTimeMillis()
    val lineitem: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "lineitem")
      .option("partitionColumn","l_orderkey")
      .option("lowerBound",lowerBound)
      .option("upperBound",upperBound)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    spark.udf.register("myUdf", new MyAvg)

    val input: DataFrame = spark.sql(
      """select
        |        l_returnflag,
        |        l_linestatus,
        |        myUdf(l_orderkey),
        |        sum(l_quantity) as sum_qty,
        |        sum(l_extendedprice) as sum_base_price,
        |        sum(l_extendedprice * (1 - l_discount)) as sum_disc_price,
        |        sum(l_extendedprice * (1 - l_discount) * (1 + l_tax)) as sum_charge,
        |        avg(l_quantity) as avg_qty,
        |        avg(l_extendedprice) as avg_price,
        |        avg(l_discount) as avg_disc,
        |        count(*) as count_order
        |from
        |        lineitem
        |where
        |        l_shipdate <= date '1998-12-01'
        |group by
        |        l_returnflag,
        |        l_linestatus
        |order by
        |        l_returnflag,
        |        l_linestatus""".stripMargin)
    val condition = true
    var result: Dataset[(String,Double)] = null
    if (condition) {
      result = input.map(x => {
        Tuple2(x.getString(0), x.getDouble(2))
      })
    } else {
      result = input.map(x => Tuple2(x.getString(1), x.getDouble(2)))
    }
    result.show()
    val two = System.currentTimeMillis()
    println("Total execution time is " + 1.0*(two - one)/1000)
  }
}
