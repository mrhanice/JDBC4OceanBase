package parallel;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class PartitionWithoutLoadMultiSQL {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
//        String DB_URL = "jdbc:mysql://localhost:3306/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
//        String USER = "root";
        String PASS = "123456";
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        Class.forName(JDBC_DRIVER);
        long now = System.currentTimeMillis();
        String sql = querys.get(Integer.parseInt(args[1]));
        int taskNum = Integer.parseInt(args[2]);
        CountDownLatch latch = new CountDownLatch(taskNum);
        int total = 96;
        int split = total/taskNum;
        int fieldsNum = Integer.parseInt(args[3]);

        for(int i = 0; i < taskNum; i++) {
            String partitionFiled = "partition (";
            for(int j = i*split; j < (i+1)*split; j++) {
                partitionFiled += "p" + j + " ";
            }
            partitionFiled += ")";
            new RunJDBC(DB_URL,USER,PASS,String.format(sql,partitionFiled),latch,i,fieldsNum).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run" + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    static void addQuery(ArrayList<String> querys) {
        //tpch q1
        querys.add("select /*+    parallel(10) */\n" +
                "        l_returnflag,\n" +
                "        l_linestatus,\n" +
                "        sum(l_quantity) as sum_qty,\n" +
                "        sum(l_extendedprice) as sum_base_price,\n" +
                "        sum(l_extendedprice * (1 - l_discount)) as sum_disc_price,\n" +
                "        sum(l_extendedprice * (1 - l_discount) * (1 + l_tax)) as sum_charge,\n" +
                "        avg(l_quantity) as avg_qty,\n" +
                "        avg(l_extendedprice) as avg_price,\n" +
                "        avg(l_discount) as avg_disc,\n" +
                "        count(*) as count_order\n" +
                "from\n" +
                "        lineitem %s \n" +
                "where\n" +
                "        l_shipdate <= date '1998-12-01'\n" +
                "group by\n" +
                "        l_returnflag,\n" +
                "        l_linestatus\n" +
                "order by\n" +
                "        l_returnflag,\n" +
                "        l_linestatus;");

        //tpch q2
        querys.add("select /*+    parallel(10) */ \n" +
                "        s_acctbal,\n" +
                "        s_name,\n" +
                "        n_name,\n" +
                "        p_partkey,\n" +
                "        p_mfgr,\n" +
                "        s_address,\n" +
                "        s_phone,\n" +
                "        s_comment\n" +
                "from\n" +
                "        part,\n" +
                "        supplier,\n" +
                "        partsupp,\n" +
                "        nation,\n" +
                "        region\n" +
                "where\n" +
                "        p_partkey = ps_partkey\n" +
                "        and s_suppkey = ps_suppkey\n" +
                "        and p_size = 15\n" +
                "        and p_type like '%BRASS'\n" +
                "        and s_nationkey = n_nationkey\n" +
                "        and n_regionkey = r_regionkey\n" +
                "        and r_name = 'EUROPE'\n" +
                "        and ps_supplycost = (\n" +
                "                select\n" +
                "                        min(ps_supplycost)\n" +
                "                from\n" +
                "                        partsupp,\n" +
                "                        supplier,\n" +
                "                        nation,\n" +
                "                        region\n" +
                "                where\n" +
                "                        p_partkey = ps_partkey\n" +
                "                        and s_suppkey = ps_suppkey\n" +
                "                        and s_nationkey = n_nationkey\n" +
                "                        and n_regionkey = r_regionkey\n" +
                "                        and r_name = 'EUROPE'\n" +
                "        )\n" +
                "order by\n" +
                "        s_acctbal desc,\n" +
                "        n_name,\n" +
                "        s_name,\n" +
                "        p_partkey;");

        //tpch q3
        querys.add("select /*+    parallel(10) */ \n" +
                "        l_orderkey,\n" +
                "        sum(l_extendedprice * (1 - l_discount)) as revenue,\n" +
                "        o_orderdate,\n" +
                "        o_shippriority\n" +
                "from\n" +
                "        customer,\n" +
                "        orders,\n" +
                "        lineitem %s \n" +
                "where\n" +
                "        c_mktsegment = 'BUILDING'\n" +
                "        and c_custkey = o_custkey\n" +
                "        and l_orderkey = o_orderkey\n" +
                "        and o_orderdate < date '1995-03-15'\n" +
                "        and l_shipdate > date '1995-03-15'\n" +
                "group by\n" +
                "        l_orderkey,\n" +
                "        o_orderdate,\n" +
                "        o_shippriority\n" +
                "order by\n" +
                "        revenue desc,\n" +
                "        o_orderdate;");

        //tpch q4
        querys.add("select /*+    parallel(10) */ \n" +
                "        o_orderpriority,\n" +
                "        count(*) as order_count\n" +
                "from\n" +
                "        orders\n" +
                "where\n" +
                "        o_orderdate >= date '1993-07-01'\n" +
                "        and o_orderdate < date '1993-07-01' + interval '3' month\n" +
                "        and exists (\n" +
                "                select\n" +
                "                        *\n" +
                "                from\n" +
                "                        lineitem\n" +
                "                where\n" +
                "                        l_orderkey = o_orderkey\n" +
                "                        and l_commitdate < l_receiptdate\n" +
                "        )\n" +
                "group by\n" +
                "        o_orderpriority\n" +
                "order by\n" +
                "        o_orderpriority;");

        //tpch q5
        querys.add("select /*+    parallel(10) */ \n" +
                "        n_name,\n" +
                "        sum(l_extendedprice * (1 - l_discount)) as revenue\n" +
                "from\n" +
                "        customer,\n" +
                "        orders,\n" +
                "        lineitem %s ,\n" +
                "        supplier,\n" +
                "        nation,\n" +
                "        region\n" +
                "where\n" +
                "        c_custkey = o_custkey\n" +
                "        and l_orderkey = o_orderkey\n" +
                "        and l_suppkey = s_suppkey\n" +
                "        and c_nationkey = s_nationkey\n" +
                "        and s_nationkey = n_nationkey\n" +
                "        and n_regionkey = r_regionkey\n" +
                "        and r_name = 'ASIA'\n" +
                "        and o_orderdate >= date '1994-01-01'\n" +
                "        and o_orderdate < date '1994-01-01' + interval '1' year\n" +
                "group by\n" +
                "        n_name\n" +
                "order by\n" +
                "        revenue desc;");

        // tpch q6
        querys.add("select  /*+    parallel(10) */\n" +
                "        sum(l_extendedprice * l_discount) as revenue\n" +
                "from\n" +
                "        lineitem %s\n" +
                "where\n" +
                "        l_shipdate >= date '1994-01-01'\n" +
                "        and l_shipdate < date '1994-01-01' + interval '1' year\n" +
                "        and l_discount between .06 - 0.01 and .06 + 0.01\n" +
                "        and l_quantity < 24;");

        //tpch q7
        querys.add("select /*+    parallel(10) */\n" +
                "        supp_nation,\n" +
                "        cust_nation,\n" +
                "        l_year,\n" +
                "        sum(volume) as revenue\n" +
                "from\n" +
                "        (\n" +
                "                select\n" +
                "                        n1.n_name as supp_nation,\n" +
                "                        n2.n_name as cust_nation,\n" +
                "                        extract(year from l_shipdate) as l_year,\n" +
                "                        l_extendedprice * (1 - l_discount) as volume\n" +
                "                from\n" +
                "                        supplier,\n" +
                "                        lineitem %s ,\n" +
                "                        orders,\n" +
                "                        customer,\n" +
                "                        nation n1,\n" +
                "                        nation n2\n" +
                "                where\n" +
                "                        s_suppkey = l_suppkey\n" +
                "                        and o_orderkey = l_orderkey\n" +
                "                        and c_custkey = o_custkey\n" +
                "                        and s_nationkey = n1.n_nationkey\n" +
                "                        and c_nationkey = n2.n_nationkey\n" +
                "                        and (\n" +
                "                                (n1.n_name = 'FRANCE' and n2.n_name = 'GERMANY')\n" +
                "                                or (n1.n_name = 'GERMANY' and n2.n_name = 'FRANCE')\n" +
                "                        )\n" +
                "                        and l_shipdate between date '1995-01-01' and date '1996-12-31'\n" +
                "        ) as shipping\n" +
                "group by\n" +
                "        supp_nation,\n" +
                "        cust_nation,\n" +
                "        l_year\n" +
                "order by\n" +
                "        supp_nation,\n" +
                "        cust_nation,\n" +
                "        l_year;");

        //tpch q8
        querys.add("select /*+    parallel(10) */\n" +
                "        o_year,\n" +
                "        sum(case\n" +
                "                when nation = 'BRAZIL' then volume\n" +
                "                else 0\n" +
                "        end) / sum(volume) as mkt_share\n" +
                "from\n" +
                "        (\n" +
                "                select\n" +
                "                        extract(year from o_orderdate) as o_year,\n" +
                "                        l_extendedprice * (1 - l_discount) as volume,\n" +
                "                        n2.n_name as nation\n" +
                "                from\n" +
                "                        part,\n" +
                "                        supplier,\n" +
                "                        lineitem %s ,\n" +
                "                        orders,\n" +
                "                        customer,\n" +
                "                        nation n1,\n" +
                "                        nation n2,\n" +
                "                        region\n" +
                "                where\n" +
                "                        p_partkey = l_partkey\n" +
                "                        and s_suppkey = l_suppkey\n" +
                "                        and l_orderkey = o_orderkey\n" +
                "                        and o_custkey = c_custkey\n" +
                "                        and c_nationkey = n1.n_nationkey\n" +
                "                        and n1.n_regionkey = r_regionkey\n" +
                "                        and r_name = 'AMERICA'\n" +
                "                        and s_nationkey = n2.n_nationkey\n" +
                "                        and o_orderdate between date '1995-01-01' and date '1996-12-31'\n" +
                "                        and p_type = 'ECONOMY ANODIZED STEEL'\n" +
                "        ) as all_nations\n" +
                "group by\n" +
                "        o_year\n" +
                "order by\n" +
                "        o_year;");

        //tpch q9
        querys.add("select /*+    parallel(10) */\n" +
                "        nation,\n" +
                "        o_year,\n" +
                "        sum(amount) as sum_profit\n" +
                "from\n" +
                "        (\n" +
                "                select\n" +
                "                        n_name as nation,\n" +
                "                        extract(year from o_orderdate) as o_year,\n" +
                "                        l_extendedprice * (1 - l_discount) - ps_supplycost * l_quantity as amount\n" +
                "                from\n" +
                "                        part,\n" +
                "                        supplier,\n" +
                "                        lineitem %s ,\n" +
                "                        partsupp,\n" +
                "                        orders,\n" +
                "                        nation\n" +
                "                where\n" +
                "                        s_suppkey = l_suppkey\n" +
                "                        and ps_suppkey = l_suppkey\n" +
                "                        and ps_partkey = l_partkey\n" +
                "                        and p_partkey = l_partkey\n" +
                "                        and o_orderkey = l_orderkey\n" +
                "                        and s_nationkey = n_nationkey\n" +
                "                        and p_name like '%%green%%'\n" +
                "        ) as profit\n" +
                "group by\n" +
                "        nation,\n" +
                "        o_year\n" +
                "order by\n" +
                "        nation,\n" +
                "        o_year desc;");

        //tpch q10
        querys.add("select /*+    parallel(10) */\n" +
                "        c_custkey,\n" +
                "        c_name,\n" +
                "        sum(l_extendedprice * (1 - l_discount)) as revenue,\n" +
                "        c_acctbal,\n" +
                "        n_name,\n" +
                "        c_address,\n" +
                "        c_phone,\n" +
                "        c_comment\n" +
                "from\n" +
                "        customer,\n" +
                "        orders,\n" +
                "        lineitem %s ,\n" +
                "        nation\n" +
                "where\n" +
                "        c_custkey = o_custkey\n" +
                "        and l_orderkey = o_orderkey\n" +
                "        and o_orderdate >= date '1993-10-01'\n" +
                "        and o_orderdate < date '1993-10-01' + interval '3' month\n" +
                "        and l_returnflag = 'R'\n" +
                "        and c_nationkey = n_nationkey\n" +
                "group by\n" +
                "        c_custkey,\n" +
                "        c_name,\n" +
                "        c_acctbal,\n" +
                "        c_phone,\n" +
                "        n_name,\n" +
                "        c_address,\n" +
                "        c_comment\n" +
                "order by\n" +
                "        revenue desc;");
    }
}
