package parallel;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class Motivation {

    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
//        String DB_URL = "jdbc:mysql://localhost:3306/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
//        String USER = "root";
        String PASS = "123456";
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        String sql = querys.get(Integer.parseInt(args[1]));
        int taskNum = Integer.parseInt(args[2]);
        CountDownLatch latch = new CountDownLatch(8);
        int total = Integer.parseInt(args[3]);
        int split = total/taskNum;
        int fieldsNum = Integer.parseInt(args[4]);

        for(int i = 0; i < 8; i++) {
            new RunJDBC(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,fieldsNum).start();
//            new parallel.RunJDBC(DB_URL,USER,PASS,String.format(sql,0,600000),latch).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run " + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        //tpch q1
        querys.add("select /*+ parallel(20) */\n" +
                "    l_extendedprice, l_discount, l_tax, l_quantity,\n" +
                "    o_totalprice,\n" +
                "    c_custkey, c_acctbal, c_nationkey\n" +
                "from\n" +
                "    lineitem,\n" +
                "    orders,\n" +
                "    customer\n" +
                "where\n" +
                "    l_orderkey = o_orderkey\n" +
                "    and c_custkey = o_custkey\n" +
                "    and o_orderdate >= date '1994-01-01'\n" +
                "    and o_orderdate < date '1994-01-01' + interval '2' year\n" +
                "    and l_orderkey >= %s and l_orderkey < %s\n" +
                "    and o_orderkey >= %s and o_orderkey < %s");
    }
}
