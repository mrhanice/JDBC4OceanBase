package parallel;

import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class RunJDBC extends Thread {
    private String DB_URL;
    private String USER;
    private String PASS;
    private String sql;
    private CountDownLatch latch;
    private int id;
    private int fieldsNum;

    public RunJDBC(String DB_URL,String USER,String PASS,String sql,CountDownLatch latch,int id,int fieldsNum) {
        this.DB_URL = DB_URL;
        this.USER = USER;
        this.PASS = PASS;
        this.sql = sql;
        this.latch = latch;
        this.id = id;
        this.fieldsNum = fieldsNum;
    }

    public RunJDBC(){}

    public void run() {
        Connection conn = null;
        PreparedStatement pst = null;
        try{
            System.out.println(sql);
            long one = System.currentTimeMillis();
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 " + id);
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();

            int cnt = 0;
            while(rs.next()) {
//                ArrayList<Object> arr = new ArrayList<Object>();
                for(int i = 1; i <= fieldsNum; i++) {
                    rs.getObject(i);
                }
                cnt++;
            }
            long two = System.currentTimeMillis();
            System.out.println("Thread " + id + " jdbc time is " + 1.0*(two - one)/1000);
            System.out.println("The number of rows of thread " + id + " is " + cnt);
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            latch.countDown();
            System.out.println("finished latch - 1");
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}
