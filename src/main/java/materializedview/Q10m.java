package materializedview;

import parallel.RunJDBC;

import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class Q10m {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
        String PASS = "123456";
        ArrayList<String> querys = new ArrayList<String>();
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        String dropTable = "DROP TABLE IF EXISTS q10_3join_v;";
        String createTable =
                "CREATE TABLE q10_3join_v (\n" +
                "  c_custkey BIGINT(20) NOT NULL,\n" +
                "  c_name VARCHAR(25) NULL,\n" +
                "  c_address VARCHAR(40) NULL,\n" +
                "  c_phone CHAR(15) NULL,\n" +
                "  c_acctbal BIGINT(20) NULL,\n" +
                "  c_comment VARCHAR(117) NULL,\n" +
                "  o_orderkey BIGINT(20) NOT NULL,\n" +
                "  n_name CHAR(25) NULL,\n" +
                "  INDEX o_id(o_orderkey)\n" +
                "  );";
        String insertJoinResult =
                "INSERT INTO q10_3join_v (SELECT /*+    parallel(96) */ \n" +
                "\tc_custkey, \n" +
                "\tc_name,\n" +
                "\tc_address,\n" +
                "\tc_phone,\n" +
                "\tc_acctbal,\n" +
                "\tc_comment,\n" +
                "\to_orderkey,\n" +
                "\tn_name\n" +
                "FROM \n" +
                "\tcustomer,\n" +
                "\torders,\n" +
                "\tnation\n" +
                "WHERE \n" +
                "\tc_custkey=o_custkey\n" +
                "\tAND c_nationkey=n_nationkey\n" +
                "\tAND o_orderdate >= DATE '1993-10-01'\n" +
                "\tAND o_orderdate < DATE '1993-10-01' + INTERVAL '3' MONTH);";
        executeSQL(DB_URL,USER,PASS,dropTable,1);
        executeSQL(DB_URL,USER,PASS,createTable,1);
        executeSQL(DB_URL,USER,PASS,insertJoinResult,2);
        long two = System.currentTimeMillis();
        System.out.println("Time of insertJoinResult is = " + 1.0*(two - now)/1000);
        String join =
                "SELECT /*+    parallel(10) */ \n" +
                "        c_custkey,\n" +
                "        c_name,\n" +
                "        c_acctbal,\n" +
                "        n_name,\n" +
                "        c_address,\n" +
                "        c_phone,\n" +
                "        c_comment,\n" +
                "        l_extendedprice,\n" +
                "        l_discount\n" +
                " FROM lineitem \n" +
                " JOIN  q10_3join_v ON l_orderkey = o_orderkey WHERE l_returnflag = 'R' and l_orderkey > %d and l_orderkey < %d;";

        int taskNum = Integer.parseInt(args[1]);
        CountDownLatch latch = new CountDownLatch(taskNum);
        int total = Integer.parseInt(args[2]);
        int split = total/taskNum;
        int fieldsNum = Integer.parseInt(args[3]);

        for(int i = 0; i < taskNum; i++) {
            new RunJDBC(DB_URL,USER,PASS,String.format(join,i*split,(i+1)*split),latch,i,fieldsNum).start();
//            new parallel.RunJDBC(DB_URL,USER,PASS,String.format(sql,0,600000),latch).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run" + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    static void executeSQL(String DB_URL,String USER,String PASS,String sql,int flag) {
        Connection conn = null;
        PreparedStatement pst = null;
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            pst = conn.prepareStatement(sql);
            if(flag == 1) {
                boolean exe = pst.execute();
                if(!exe) {
                    System.out.println(sql + " successfully");
                }
            } else if(flag == 2) {
                int cnt = pst.executeUpdate();
                conn.commit();
                System.out.println("update " + cnt  + " rows");
            } else if(flag == 3) {
                ResultSet resultSet = pst.executeQuery();
                while(resultSet.next()) {
                    System.out.println(resultSet.getInt(1) + " ");
                }
            }
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}
