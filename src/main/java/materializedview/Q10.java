package materializedview;

import parallel.SingleSQLWithoutLoad;

import java.sql.*;
import java.util.ArrayList;

public class Q10 {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
//        String DB_URL = "jdbc:mysql://localhost:3306/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
//        String USER = "root";
        String PASS = "123456";

        ArrayList<String> querys = new ArrayList<String>();
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        String sql = "SELECT  /*+    parallel(96) */\n" +
                "        c_custkey,\n" +
                "        c_name,\n" +
                "        c_acctbal,\n" +
                "        n_name,\n" +
                "        c_address,\n" +
                "        c_phone,\n" +
                "        c_comment,\n" +
                "        l_extendedprice,\n" +
                "        l_discount \n" +
                "FROM\n" +
                "        customer,\n" +
                "        orders,\n" +
                "        nation,\n" +
                "        lineitem\n" +
                "WHERE\n" +
                "        c_custkey = o_custkey\n" +
                "        AND l_orderkey = o_orderkey\n" +
                "        AND o_orderdate >= DATE '1993-10-01'\n" +
                "        AND o_orderdate < DATE '1993-10-01' + INTERVAL '3' MONTH\n" +
                "        AND l_returnflag = 'R'\n" +
                "        AND c_nationkey = n_nationkey;";
        int fieldNums = Integer.parseInt(args[1]);
        Connection conn = null;
        PreparedStatement pst = null;
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            System.out.println(sql);
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            int cnt = 0;
            while(rs.next()) {
                for(int i = 1; i <= fieldNums; i++) {
                    Object t = rs.getObject(i);
                }
                cnt++;
            }
            System.out.println("Cnt = " + cnt);
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }

        long next = System.currentTimeMillis();
        System.out.println("Time of executing sql is = " + 1.0*(next - now)/1000);
    }
}
