package semanticparallel.group;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

class RunTwoTableJoinGroup extends Thread {
    private String DB_URL;
    private String USER;
    private String PASS;
    private String sql;
    private CountDownLatch latch;
    private int id;
    private ArrayList<GresultT0> al0 = null;
    private ArrayList<GresultT1> al1 = null;

    public RunTwoTableJoinGroup(String DB_URL,String USER,String PASS,String sql,CountDownLatch latch,int id,ArrayList<GresultT0> al0,ArrayList<GresultT1> al1) {
        this.DB_URL = DB_URL;
        this.USER = USER;
        this.PASS = PASS;
        this.sql = sql;
        this.latch = latch;
        this.id = id;
        this.al0 = al0;
        this.al1 = al1;
    }

    public RunTwoTableJoinGroup(){}

    public void run() {
        Connection conn = null;
        PreparedStatement pst = null;
        try{
//            System.out.println(sql);
            long one = System.currentTimeMillis();
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 " + id);
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                if(al0 != null) {
                    al0.add(new GresultT0(rs.getInt(1),rs.getDouble(2),rs.getString(3),rs.getString(4)));
                } else if(al1 != null) {
                    al1.add(new GresultT1(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4)));
                }
            }
            long two = System.currentTimeMillis();
            System.out.println("Thread " + id + " jdbc time is " + 1.0*(two - one)/1000);
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            latch.countDown();
            System.out.println("finished latch - 1");
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}

public class TwoTableJoinGroupParallel {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);
        String USER = "root@test_mrhanice";
        String PASS = "123456";
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);
        int taskNum = Integer.parseInt(args[2]);
        CountDownLatch latch = new CountDownLatch(taskNum);
        int total = Integer.parseInt(args[3]);
        int split = total/taskNum;
        ArrayList<GresultT0>[] ans0 = new ArrayList[taskNum];
        ArrayList<GresultT1>[] ans1 = new ArrayList[taskNum];
        if(sqlId == 0) {
            for(int i = 0; i < taskNum; i++) {
                ans0[i] = new ArrayList<GresultT0>();
            }
            for(int i = 0; i < taskNum; i++) {
                System.out.println(String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split));
                new RunTwoTableJoinGroup(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,ans0[i],null).start();
            }
        } else {
            for(int i = 0; i < taskNum; i++) {
                ans1[i] = new ArrayList<GresultT1>();
            }
            for(int i = 0; i < taskNum; i++) {
                System.out.println(String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split));
                new RunTwoTableJoinGroup(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,null,ans1[i]).start();
            }
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long runfinish = System.currentTimeMillis();
        System.out.println("Time of Run " + " parallel is = " + 1.0*(runfinish - now)/1000);
        ArrayList<GresultT0> fal0 = new ArrayList<GresultT0>();
        ArrayList<GresultT1> fal1 = new ArrayList<GresultT1>();
        if (sqlId == 0) {
            for(int i = 0; i < taskNum; i++) {
                for(int j = 0; j < ans0[i].size(); j++) {
                    fal0.add(ans0[i].get(j));
                }
            }
            Collections.sort(fal0);
            System.out.println("size = " + fal0.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + fal0.get(i).l_orderkey + " " + fal0.get(i).revenue + " " + fal0.get(i).o_orderdate + " " + fal0.get(i).o_shippriority);
            }
        } else if(sqlId == 1) {
            for(int i = 0; i < taskNum; i++) {
                for(int j = 0; j < ans1[i].size(); j++) {
                    fal1.add(ans1[i].get(j));
                }
            }
            Collections.sort(fal1);
            System.out.println("size = " + fal1.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + fal1.get(i).l_orderkey + " " + fal1.get(i).l_shipmode + " " + fal1.get(i).high_line_count + " " + fal1.get(i).low_line_count);
            }
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run" + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(20) */\n" +
                        "l_orderkey,\n" +
                        "SUM(l_extendedprice * (1 - l_discount)) AS revenue,\n" +
                        "o_orderdate,\n" +
                        "o_shippriority\n" +
                "FROM\n" +
                        "orders,\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "l_orderkey = o_orderkey\n" +
                        "AND o_orderdate < DATE '1995-03-15'\n" +
                        "AND l_shipdate > DATE '1995-03-15'\n" +
                        "AND l_orderkey >= %d \n" +
                        "AND l_orderkey < %d \n" +
                        "AND o_orderkey >= %d \n" +
                        "AND o_orderkey < %d \n" +
                "GROUP BY\n" +
                        "l_orderkey,\n" +
                        "o_orderdate,\n" +
                        "o_shippriority\n" +
                "ORDER BY\n" +
                        "l_orderkey;");
        querys.add(
                "SELECT /*+  parallel(20) */\n" +
                        "l_orderkey,\n" +
                        "l_shipmode,\n" +
                        "SUM(CASE\n" +
                        "WHEN o_orderpriority = '1-URGENT'\n" +
                        "OR o_orderpriority = '2-HIGH'\n" +
                        "THEN 1\n" +
                        "ELSE 0\n" +
                        "END) AS high_line_count,\n" +
                        "SUM(CASE\n" +
                        "WHEN o_orderpriority <> '1-URGENT'\n" +
                        "AND o_orderpriority <> '2-HIGH'\n" +
                        "THEN 1\n" +
                        "ELSE 0\n" +
                        "END) AS low_line_count\n" +
                "FROM\n" +
                        "orders,\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "o_orderkey = l_orderkey\n" +
                        "AND l_shipmode IN ('MAIL', 'SHIP')\n" +
                        "AND l_commitdate < l_receiptdate\n" +
                        "AND l_shipdate < l_commitdate\n" +
                        "AND l_receiptdate >= DATE '1994-01-01'\n" +
                        "AND l_receiptdate < DATE '1994-01-01' + INTERVAL '1' YEAR\n" +
                        "AND l_orderkey >= %d \n" +
                        "AND l_orderkey < %d \n" +
                        "AND o_orderkey >= %d \n" +
                        "AND o_orderkey < %d \n" +
                "GROUP BY\n" +
                        "l_orderkey,\n" +
                        "l_shipmode\n" +
                "ORDER BY\n" +
                        "l_orderkey;");
    }
}
