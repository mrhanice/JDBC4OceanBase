package semanticparallel.group;

import java.sql.*;
import java.util.ArrayList;

class Gresult0 implements Comparable<Gresult0> {
    int l_orderkey;
    int count;
    double partavg;
    public Gresult0(int l_orderkey,int count,double partavg) {
        this.l_orderkey = l_orderkey;
        this.count = count;
        this.partavg = partavg;
    }
    public Gresult0() {}

    public int compareTo(Gresult0 o) {
        return this.l_orderkey - o.l_orderkey;
    }
}

public class SingleTable {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
        String PASS = "123456";

        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);

        Connection conn = null;
        PreparedStatement pst = null;
        ArrayList<Gresult0> ans = new ArrayList<Gresult0>();
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                ans.add(new Gresult0(rs.getInt(1),rs.getInt(2),rs.getDouble(3)));
            }
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("size = " + ans.size());
        for(int i = 0; i < 20; i++) {
            System.out.println("" + ans.get(i).l_orderkey + " " + ans.get(i).count + " " + ans.get(i).partavg);
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of executing sql is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(192) */\n" +
                        "l_orderkey,\n" +
                        "COUNT(*) AS count_order,\n" +
                        "AVG(l_partkey)\n" +
                "FROM\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "l_shipdate <= DATE '1998-12-01' - INTERVAL '9' MONTH\n" +
                "GROUP BY\n" +
                        "l_orderkey\n" +
                "ORDER BY\n" +
                    "l_orderkey;");
        querys.add(
                "SELECT  /*+    parallel(192) */\n" +
                "   o_orderkey,\n" +
                "   SUM(CASE\n" +
                "       WHEN o_orderpriority = '1-URGENT'\n" +
                "           OR o_orderpriority = '2-HIGH'\n" +
                "               THEN 1\n" +
                "           ELSE 0\n" +
                "       END) AS high_line_count,\n" +
                "    SUM(CASE\n" +
                "       WHEN o_orderpriority <> '1-URGENT'\n" +
                "           AND o_orderpriority <> '2-HIGH'\n" +
                "           THEN 1\n" +
                "       ELSE 0\n" +
                "   END) AS low_line_count\n" +
                "FROM\n" +
                "   orders\n" +
                "WHERE\n" +
                "   o_orderstatus = 'O'\n" +
                "   AND o_orderdate >= DATE '1996-01-01'\n" +
                "   AND o_orderdate < DATE '1996-01-01' + INTERVAL '1' YEAR\n" +
                "GROUP BY\n" +
                "   o_orderkey\n" +
                "ORDER BY\n" +
                "   o_orderkey;");
    }
}
