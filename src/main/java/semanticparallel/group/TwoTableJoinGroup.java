package semanticparallel.group;

import java.sql.*;
import java.util.ArrayList;

class GresultT0 implements Comparable<GresultT0> {
    int l_orderkey;
    double revenue;
    String o_orderdate;
    String o_shippriority;
    public GresultT0(int l_orderkey,double revenue,String o_orderdate,String o_shippriority) {
        this.l_orderkey = l_orderkey;
        this.revenue = revenue;
        this.o_orderdate = o_orderdate;
        this.o_shippriority = o_shippriority;
    }
    public GresultT0(){}

    public int compareTo(GresultT0 o) {
        return this.l_orderkey - o.l_orderkey;
    }
}

class GresultT1 implements  Comparable<GresultT1> {
    int l_orderkey;
    String l_shipmode;
    int high_line_count;
    int low_line_count;
    public GresultT1(int l_orderkey,String l_shipmode,int high_line_count,int low_line_count) {
        this.l_orderkey = l_orderkey;
        this.l_shipmode = l_shipmode;
        this.high_line_count = high_line_count;
        this.low_line_count = low_line_count;
    }
    public GresultT1() {}

    public int compareTo(GresultT1 o) {
        return this.l_orderkey - o.l_orderkey;
    }
}

public class TwoTableJoinGroup {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
        String PASS = "123456";

        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);

        Connection conn = null;
        PreparedStatement pst = null;
        ArrayList<GresultT0> ans0 = new ArrayList<GresultT0>();
        ArrayList<GresultT1> ans1 = new ArrayList<GresultT1>();
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                if(sqlId == 0) {
                    ans0.add(new GresultT0(rs.getInt(1),rs.getDouble(2),rs.getString(3),rs.getString(4)));
                } else if(sqlId == 1) {
                    ans1.add(new GresultT1(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4)));
                }
            }
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            } catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        if(sqlId == 0) {
            System.out.println("size = " + ans0.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + ans0.get(i).l_orderkey + " " + ans0.get(i).revenue + " " + ans0.get(i).o_orderdate + " " + ans0.get(i).o_shippriority);
            }
        } else if(sqlId == 1) {
            System.out.println("size = " + ans1.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + ans1.get(i).l_orderkey + " " + ans1.get(i).l_shipmode + " " + ans1.get(i).high_line_count + " " + ans1.get(i).low_line_count);
            }
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of executing sql is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(192) */\n" +
                        "l_orderkey,\n" +
                        "SUM(l_extendedprice * (1 - l_discount)) AS revenue,\n" +
                        "o_orderdate,\n" +
                        "o_shippriority\n" +
                "FROM\n" +
                        "orders,\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "l_orderkey = o_orderkey\n" +
                        "AND o_orderdate < DATE '1995-03-15'\n" +
                        "AND l_shipdate > DATE '1995-03-15'\n" +
                "GROUP BY\n" +
                        "l_orderkey,\n" +
                        "o_orderdate,\n" +
                        "o_shippriority\n" +
                "ORDER BY\n" +
                        "l_orderkey;");
        querys.add(
                "SELECT /*+  parallel(192) */\n" +
                        "l_orderkey,\n" +
                        "l_shipmode,\n" +
                        "SUM(CASE\n" +
                        "WHEN o_orderpriority = '1-URGENT'\n" +
                        "OR o_orderpriority = '2-HIGH'\n" +
                        "THEN 1\n" +
                        "ELSE 0\n" +
                        "END) AS high_line_count,\n" +
                        "SUM(CASE\n" +
                        "WHEN o_orderpriority <> '1-URGENT'\n" +
                        "AND o_orderpriority <> '2-HIGH'\n" +
                        "THEN 1\n" +
                        "ELSE 0\n" +
                        "END) AS low_line_count\n" +
                "FROM\n" +
                        "orders,\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "o_orderkey = l_orderkey\n" +
                        "AND l_shipmode IN ('MAIL', 'SHIP')\n" +
                        "AND l_commitdate < l_receiptdate\n" +
                        "AND l_shipdate < l_commitdate\n" +
                        "AND l_receiptdate >= DATE '1994-01-01'\n" +
                        "AND l_receiptdate < DATE '1994-01-01' + INTERVAL '1' YEAR\n" +
                "GROUP BY\n" +
                        "l_orderkey,\n" +
                        "l_shipmode\n" +
                "ORDER BY\n" +
                        "l_orderkey;");
    }
}
