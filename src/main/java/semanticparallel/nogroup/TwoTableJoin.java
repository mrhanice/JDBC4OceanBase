package semanticparallel.nogroup;

import java.sql.*;
import java.util.ArrayList;

class Result1 implements Comparable<Result1> {
    public int l_orderkey;
    public String o_orderdate;
    public String o_shippriority;
    public Result1(int l_orderkey,String o_orderdate,String o_shippriority) {
        this.l_orderkey = l_orderkey;
        this.o_orderdate = o_orderdate;
        this.o_shippriority = o_shippriority;
    }

    public int compareTo(Result1 o) {
        return this.l_orderkey - o.l_orderkey;
    }
}

public class TwoTableJoin {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
        String PASS = "123456";

        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);

        Connection conn = null;
        PreparedStatement pst = null;
        ArrayList<Result1> ans = new ArrayList<Result1>();
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                ans.add(new Result1(rs.getInt(1),rs.getString(2),rs.getString(3)));
            }
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("size = " + ans.size());
        for(int i = 0; i < 20; i++) {
            System.out.println("" + ans.get(i).l_orderkey + " " + ans.get(i).o_orderdate + " " + ans.get(i).o_shippriority);
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of executing sql is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "select /*+    parallel(192) */\n" +
                "   l_orderkey,\n" +
                "   o_orderdate,\n" +
                "   o_shippriority\n" +
                "from\n" +
                "   orders,\n" +
                "   lineitem\n" +
                "where\n" +
                "   l_orderkey = o_orderkey\n" +
                "   and o_orderdate < date '1995-03-15'\n" +
                "   and l_shipdate > date '1995-03-15'\n" +
                "order by\n" +
                "   l_orderkey");
        querys.add("select /*+    parallel(192) */\n" +
                "        l_orderkey,\n" +
                "        l_shipmode,\n" +
                "        l_receiptdate\n" +
                "from\n" +
                "        orders,\n" +
                "        lineitem\n" +
                "where\n" +
                "        o_orderkey = l_orderkey\n" +
                "        and l_shipmode in ('MAIL', 'SHIP')\n" +
                "        and l_commitdate < l_receiptdate\n" +
                "        and l_shipdate < l_commitdate\n" +
                "        and l_receiptdate >= date '1994-01-01'\n" +
                "        and l_receiptdate < date '1994-01-01' + interval '1' year\n" +
                "order by\n" +
                "   l_orderkey");
    }
}
