package semanticparallel.nogroup

import org.apache.spark.sql.catalyst.plans.logical.LogicalPlan
import org.apache.spark.sql.{DataFrame, SparkSession}
import udaf.MyAvg

object ShowPlan {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.
      builder()
      .master("local[*]")
      .appName("Case2")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    val database = args(0) //String database = args[0];
    val numPartitions = args(1).toInt //int numPartitions = Integer.parseInt(args[5]);

    val one = System.currentTimeMillis()
    val lineitem: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "lineitem")
      .option("partitionsOld", true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    lineitem.createOrReplaceTempView("lineitem")

    val orders: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "orders")
      .option("partitionsOld", true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    orders.createOrReplaceTempView("orders")

    val customer: DataFrame = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.11.6.18:2883/" + database)
      .option("dbtable", "customer")
      .option("partitionsOld", true)
      .option("numPartitions", numPartitions)
      .option("user", "root@test_mrhanice")
      .option("password", "123456")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("fetchsize", 1000)
      .loadAutoParallel()
    customer.createOrReplaceTempView("customer")

    spark.udf.register("myUdf", new MyAvg)
    val input: DataFrame = spark.sql(
      """select
        |	o_orderpriority,
        |	count(*) as order_count,
        | myUdf(o_orderkey)
        |from
        |	orders
        |where
        |	o_orderdate >= date '1993-07-01'
        |	and o_orderdate < date '1993-07-01' + interval '3' month
        |	and exists (
        |		select
        |			*
        |		from
        |			lineitem
        |		where
        |			l_orderkey = o_orderkey
        |			and l_commitdate < l_receiptdate
        |	)
        |group by
        |	o_orderpriority
        |order by
        |	o_orderpriority""".stripMargin)
    val plan: LogicalPlan = input.queryExecution.optimizedPlan
    println(plan)
  }
}
