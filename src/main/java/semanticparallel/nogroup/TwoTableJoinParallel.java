package semanticparallel.nogroup;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

class RunTwoTableJoin extends Thread {
    private String DB_URL;
    private String USER;
    private String PASS;
    private String sql;
    private CountDownLatch latch;
    private int id;
    private ArrayList<Result1> al = null;

    public RunTwoTableJoin(String DB_URL,String USER,String PASS,String sql,CountDownLatch latch,int id,ArrayList<Result1> al) {
        this.DB_URL = DB_URL;
        this.USER = USER;
        this.PASS = PASS;
        this.sql = sql;
        this.latch = latch;
        this.id = id;
        this.al = al;
    }

    public RunTwoTableJoin(){}

    public void run() {
        Connection conn = null;
        PreparedStatement pst = null;
        try{
//            System.out.println(sql);
            long one = System.currentTimeMillis();
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 " + id);
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                al.add(new Result1(rs.getInt(1),rs.getString(2),rs.getString(3)));
            }
            long two = System.currentTimeMillis();
            System.out.println("Thread " + id + " jdbc time is " + 1.0*(two - one)/1000);
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            latch.countDown();
            System.out.println("finished latch - 1");
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}

public class TwoTableJoinParallel {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);
        String USER = "root@test_mrhanice";
        String PASS = "123456";
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);
        int taskNum = Integer.parseInt(args[2]);
        CountDownLatch latch = new CountDownLatch(taskNum);
        int total = Integer.parseInt(args[3]);
        int split = total/taskNum;
        ArrayList<Result1>[] ans = new ArrayList[taskNum];
        for(int i = 0; i < taskNum; i++) {
            ans[i] = new ArrayList<Result1>();
        }
        for(int i = 0; i < taskNum; i++) {
            System.out.println(String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split));
            new RunTwoTableJoin(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,ans[i]).start();
//            new parallel.RunJDBC(DB_URL,USER,PASS,String.format(sql,0,600000),latch).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long para = System.currentTimeMillis();
        System.out.println("Time of Run" + " parallel is = " + 1.0*(para - now)/1000);
        ArrayList<Result1> fal = new ArrayList<Result1>();
        for(int i = 0; i < taskNum; i++) {
            for(int j = 0; j < ans[i].size(); j++) {
                fal.add(ans[i].get(j));
            }
        }
        Collections.sort(fal);
        System.out.println("size = " + fal.size());
        for(int i = 0; i < 20; i++) {
            System.out.println("" + fal.get(i).l_orderkey + " " + fal.get(i).o_orderdate + " " + fal.get(i).o_shippriority);
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run" + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(20) */ +\n" +
                "   l_orderkey,\n" +
                "   o_orderdate,\n" +
                "   o_shippriority\n" +
                "FROM\n" +
                "   orders,\n" +
                "   lineitem\n" +
                "WHERE\n" +
                "   l_orderkey = o_orderkey\n" +
                "   AND o_orderdate < DATE '1995-03-15'\n" +
                "   AND l_shipdate > DATE '1995-03-15'\n" +
                "   AND l_orderkey > %d\n" +
                "   AND l_orderkey < %d\n" +
                "   AND o_orderkey > %d\n" +
                "   AND o_orderkey < %d\n" +
                "ORDER BY\n" +
                "   l_orderkey");
        querys.add("select /*+    parallel(20) */\n" +
                "        l_orderkey,\n" +
                "        l_shipmode,\n" +
                "        l_receiptdate\n" +
                "from\n" +
                "        orders,\n" +
                "        lineitem\n" +
                "where\n" +
                "        o_orderkey = l_orderkey\n" +
                "        and l_shipmode in ('MAIL', 'SHIP')\n" +
                "        and l_commitdate < l_receiptdate\n" +
                "        and l_shipdate < l_commitdate\n" +
                "        and l_receiptdate >= date '1994-01-01'\n" +
                "        and l_receiptdate < date '1994-01-01' + interval '1' year\n" +
                "        AND l_orderkey > %d\n" +
                "        AND l_orderkey < %d\n" +
                "        AND o_orderkey > %d\n" +
                "        AND o_orderkey < %d\n" +
                "order by\n" +
                "   l_orderkey");
    }
}
