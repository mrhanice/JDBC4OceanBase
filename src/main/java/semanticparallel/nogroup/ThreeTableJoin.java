package semanticparallel.nogroup;

import java.sql.*;
import java.util.ArrayList;

class Result2 implements Comparable<Result2> {
    int s_suppkey;
    int ps_partkey;
    String s_name;
    int  s_acctbal;
    String n_name;
    String s_address;
    String s_phone;
    String s_comment;
    public Result2(int s_suppkey,int ps_partkey,String s_name,int s_acctbal,String n_name,String s_address,String s_phone,String s_comment) {
        this.s_suppkey = s_suppkey;
        this.ps_partkey = ps_partkey;
        this.s_name = s_name;
        this.s_acctbal = s_acctbal;
        this.n_name = n_name;
        this.s_address = s_address;
        this.s_phone = s_phone;
        this.s_comment = s_comment;
    }
    public Result2(){}

    public int compareTo(Result2 o) {
        return this.ps_partkey - o.ps_partkey;
    }
}

public class ThreeTableJoin {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);

        String USER = "root@test_mrhanice";
        String PASS = "123456";

        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);
        Connection conn = null;
        PreparedStatement pst = null;

        ArrayList<Result1> ans0 = new ArrayList<Result1>();
        ArrayList<Result2> ans1 = new ArrayList<Result2>();
        try{
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 ");
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                if(sqlId == 0) {
                    ans0.add(new Result1(rs.getInt(1),rs.getString(2),rs.getString(3)));
                } else if(sqlId == 1) {
                    ans1.add(new Result2(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getInt(4),
                    rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)));
                }
            }
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        if(sqlId == 0) {
            System.out.println("size = " + ans0.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + ans0.get(i).l_orderkey + " " + ans0.get(i).o_orderdate + " " + ans0.get(i).o_shippriority);
            }
        } else if(sqlId == 1) {
            System.out.println("size = " + ans1.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + ans1.get(i).s_suppkey + " " + ans1.get(i).n_name + " " + ans1.get(i).ps_partkey);
            }
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of executing sql is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(192) */\n" +
                        "l_orderkey,\n" +
                        "o_orderdate,\n" +
                        "o_shippriority\n" +
                "FROM\n" +
                        "customer,\n" +
                        "orders,\n" +
                        "lineitem\n" +
                "WHERE\n" +
                        "c_mktsegment = 'BUILDING'\n" +
                        "AND c_custkey = o_custkey\n" +
                        "AND l_orderkey = o_orderkey\n" +
                        "AND o_orderdate < DATE '1995-03-15'\n" +
                        "AND l_shipdate > DATE '1995-03-15'\n" +
                "ORDER BY \n" +
                        "l_orderkey;");
        querys.add("SELECT /*+    parallel(192) */\n" +
                "        s_suppkey,\n" +
                "        ps_partkey,\n" +
                "        s_name,\n" +
                "        s_acctbal,\n" +
                "        n_name,\n" +
                "        s_address,\n" +
                "        s_phone,\n" +
                "        s_comment\n" +
                "FROM\n" +
                "        supplier,\n" +
                "        partsupp,\n" +
                "        nation\n" +
                "WHERE\n" +
                "        ps_suppkey = s_suppkey\n" +
                "        AND s_acctbal > 4000\n" +
                "        AND ps_availqty > 5000\n" +
                "        AND s_nationkey = n_nationkey\n" +
                "ORDER BY\n" +
                "        ps_partkey;");
    }
}
