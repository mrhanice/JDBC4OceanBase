package semanticparallel.nogroup;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

class RunThreeTableJoin extends Thread {
    private String DB_URL;
    private String USER;
    private String PASS;
    private String sql;
    private CountDownLatch latch;
    private int id;
    private ArrayList<Result2> al = null;

    public RunThreeTableJoin(String DB_URL,String USER,String PASS,String sql,CountDownLatch latch,int id,ArrayList<Result2> al) {
        this.DB_URL = DB_URL;
        this.USER = USER;
        this.PASS = PASS;
        this.sql = sql;
        this.latch = latch;
        this.id = id;
        this.al = al;
    }

    public RunThreeTableJoin(){}

    public void run() {
        Connection conn = null;
        PreparedStatement pst = null;
        try{
//            System.out.println(sql);
            long one = System.currentTimeMillis();
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);
            System.out.println("实例化Statement对象 " + id);
            pst = conn.prepareStatement(sql);
            pst.setFetchSize(1000);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                al.add(new Result2(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getInt(4),
                        rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)));
            }
            long two = System.currentTimeMillis();
            System.out.println("Thread " + id + " jdbc time is " + 1.0*(two - one)/1000);
            // 完成后关闭
            rs.close();
            pst.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            latch.countDown();
            System.out.println("finished latch - 1");
            // 关闭资源
            try{
                if(pst!=null) pst.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}

public class ThreeTableJoinParallel {
    public static void main(String[] args) throws ClassNotFoundException {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://10.11.6.18:2883/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
        DB_URL = String.format(DB_URL,args[0]);
        String USER = "root@test_mrhanice";
        String PASS = "123456";
        Class.forName(JDBC_DRIVER);

        long now = System.currentTimeMillis();
        ArrayList<String> querys = new ArrayList<String>();
        addQuery(querys);
        int sqlId = Integer.parseInt(args[1]);
        String sql = querys.get(sqlId);
        int taskNum = Integer.parseInt(args[2]);
        CountDownLatch latch = new CountDownLatch(taskNum);
        int total = Integer.parseInt(args[3]);
        int split = total/taskNum;
        ArrayList<Result1>[] ans0 = new ArrayList[taskNum];
        ArrayList<Result2>[] ans1 = new ArrayList[taskNum];
        if(sqlId == 0) {
            for(int i = 0; i < taskNum; i++) {
                ans0[i] = new ArrayList<Result1>();
            }
        } else if(sqlId == 1) {
            for(int i = 0; i < taskNum; i++) {
                ans1[i] = new ArrayList<Result2>();
            }
        }

        for(int i = 0; i < taskNum; i++) {
            System.out.println(String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split));
            if(sqlId == 0) {
                new RunTwoTableJoin(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,ans0[i]).start();
            } else if(sqlId == 1) {
                new RunThreeTableJoin(DB_URL,USER,PASS,String.format(sql,i*split,(i+1)*split,i*split,(i+1)*split),latch,i,ans1[i]).start();
            }
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long para = System.currentTimeMillis();
        System.out.println("Time of Run" + " parallel is = " + 1.0*(para - now)/1000);
        ArrayList<Result1> fal0 = new ArrayList<Result1>();
        ArrayList<Result2> fal1 = new ArrayList<Result2>();
        if(sqlId == 0) {
            for(int i = 0; i < taskNum; i++) {
                for(int j = 0; j < ans0[i].size(); j++) {
                    fal0.add(ans0[i].get(j));
                }
            }
            Collections.sort(fal0);
            System.out.println("size = " + fal0.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + fal0.get(i).l_orderkey + " " + fal0.get(i).o_orderdate + " " + fal0.get(i).o_shippriority);
            }
        } else if(sqlId == 1) {
            for(int i = 0; i < taskNum; i++) {
                for(int j = 0; j < ans1[i].size(); j++) {
                    fal1.add(ans1[i].get(j));
                }
            }
            Collections.sort(fal1);
            System.out.println("size = " + fal1.size());
            for(int i = 0; i < 20; i++) {
                System.out.println("" + fal1.get(i).s_suppkey + " " + fal1.get(i).n_name + " " + fal1.get(i).ps_partkey);
            }
        }
        long next = System.currentTimeMillis();
        System.out.println("Time of Run" + taskNum + " thread is = " + 1.0*(next - now)/1000);
    }

    public static void addQuery(ArrayList<String> querys) {
        querys.add(
                "SELECT /*+    parallel(20) */\n" +
                    "l_orderkey,\n" +
                    "o_orderdate,\n" +
                    "o_shippriority\n" +
                "FROM\n" +
                    "customer,\n" +
                    "orders,\n" +
                    "lineitem\n" +
                "WHERE\n" +
                    "c_mktsegment = 'BUILDING'\n" +
                    "AND c_custkey = o_custkey\n" +
                    "AND l_orderkey = o_orderkey\n" +
                    "AND o_orderdate < DATE '1995-03-15'\n" +
                    "AND l_shipdate > DATE '1995-03-15'\n" +
                    "AND l_orderkey > %d\n" +
                    "AND l_orderkey < %d\n" +
                    "AND o_orderkey > %d\n" +
                    "AND o_orderkey < %d\n" +
                "ORDER BY \n" +
                    "l_orderkey;");
        querys.add("SELECT /*+    parallel(20) */\n" +
                "        s_suppkey,\n" +
                "        ps_partkey,\n" +
                "        s_name,\n" +
                "        s_acctbal,\n" +
                "        n_name,\n" +
                "        s_address,\n" +
                "        s_phone,\n" +
                "        s_comment\n" +
                "FROM\n" +
                "        supplier,\n" +
                "        partsupp,\n" +
                "        nation\n" +
                "WHERE\n" +
                "        ps_suppkey = s_suppkey\n" +
                "        AND s_acctbal > 4000\n" +
                "        AND ps_availqty > 5000\n" +
                "        AND s_nationkey = n_nationkey\n" +
                "        AND s_suppkey >= %d\n" +
                "        AND s_suppkey < %d\n" +
                "        AND ps_suppkey >= %d\n" +
                "        AND ps_suppkey < %d\n" +
                "ORDER BY\n" +
                "        ps_partkey;");
    }
}
