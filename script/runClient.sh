#!/usr/bin/env bash

ExecutorNum=2
#ExecutorNum=72
perMem=4G
#perMem=6G
perCore=2
#perCore=3
totalCore=$(expr $ExecutorNum \* $perCore)

#master=local[*]
master=spark://ecnu05:7077
#master=spark://10.11.6.23:7177
# driver内存
#driverMem=20G
driverMem=2G
#执行模式 client or cluster
deployMod=client
# 入口类
mainClass=Client

basedir="$PWD"
#jarURl=/root/Development/JDBC4OceanBase/target/JDBCOceanBase-1.0-SNAPSHOT.jar
#jarURl=/home/mrhanice/Development/OB/JDBC4OceanBase/out/artifacts/JDBCOceanBase_jar/JDBCOceanBase.jar
#jarURl=/home/mrhanice/Development/OB/JDBC4OceanBase/target/JDBCOceanBase-1.0-SNAPSHOT.jar
jarURl=$basedir/target/JDBCOceanBase-1.0-SNAPSHOT.jar

#SPARK_HOME=/usr/local/spark-2.4.6-bin-hadoop2.7
#SPARK_HOME=/home/mrhanice/software/spark-2.4.6-bin-hadoop2.7

database="tpch_1g_part"
table="lineitem"
#partitionColumn="l_orderkey"
#lowerBound=1
#upperBound=6000000
numPartitions=3

# --jars /home/mrhanice/Development/OB/JDBC4OceanBase/lib/spark-sql_2.11-2.4.6.jar \
$SPARK_HOME/bin/spark-submit \
        --deploy-mode ${deployMod} \
        --driver-memory ${driverMem} \
        --total-executor-cores ${totalCore} \
        --executor-cores ${perCore} \
        --executor-memory ${perMem} \
        --conf spark.driver.maxResultSize=2g \
        --master ${master} \
        --class ${mainClass} ${jarURl} ${database} ${table} ${numPartitions} \
#        --class ${mainClass} ${jarURl} ${database} ${table} ${partitionColumn} ${lowerBound} ${upperBound} ${numPartitions} \
