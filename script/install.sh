#!/usr/bin/env bash

basedir="$PWD"
#echo $basedir/lib/spark-sql_2.11-2.4.6.jar

mvn install:install-file -Dfile=$basedir/lib/spark-sql_2.11-2.4.6.jar -DgroupId=com.mrhanice -DartifactId=spark-sql_2.11-2.4.6 -Dversion=1.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=$basedir/lib/spark-core_2.11-2.4.6.jar -DgroupId=com.mrhanice -DartifactId=spark-core_2.11-2.4.6 -Dversion=1.1.0 -Dpackaging=jar