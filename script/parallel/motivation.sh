#!/usr/bin/env bash

database=$1
queryId=0
taskNum=$2
total=$3
fieldsNum=$4
java -cp /usr/local/spark-2.4.6-bin-hadoop2.7/jars/mysql-connector-java-5.1.47.jar:JDBCOceanBase-1.0-SNAPSHOT.jar parallel.Motivation ${database} ${queryId} ${taskNum} ${total} ${fieldsNum}