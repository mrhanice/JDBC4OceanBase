#!/usr/bin/env bash

ExecutorNum=12
#ExecutorNum=72
perMem=6G
#perMem=6G
perCore=4
#perCore=3
totalCore=$(expr $ExecutorNum \* $perCore)

#master=local[*]
#master=spark://ecnu05:7077
master=spark://10.11.6.18:7177
# driver内存
#driverMem=2G
driverMem=6G
#执行模式 client or cluster
deployMod=client
# 入口类
mainClass=udaf.MyAvgDemoOBandSpark

basedir="$PWD"
#jarURl=/root/Development/JDBC4OceanBase/target/JDBCOceanBase-1.0-SNAPSHOT.jar
#jarURl=/home/mrhanice/Development/OB/JDBC4OceanBase/out/artifacts/JDBCOceanBase_jar/JDBCOceanBase.jar
#jarURl=/home/mrhanice/Development/OB/JDBC4OceanBase/target/JDBCOceanBase-1.0-SNAPSHOT.jar
jarURl=$basedir/target/JDBCOceanBase-1.0-SNAPSHOT.jar

#SPARK_HOME=/usr/local/spark-2.4.6-bin-hadoop2.7
#SPARK_HOME=/home/mrhanice/software/spark-2.4.6-bin-hadoop2.7

database=$1
table="lineitem"
#partitionColumn="l_orderkey"
#lowerBound=1
#upperBound=$2
numPartitions=${totalCore}
partitionType=$2

# --jars /home/mrhanice/Development/OB/JDBC4OceanBase/lib/spark-sql_2.11-2.4.6.jar \
$SPARK_HOME/bin/spark-submit \
        --deploy-mode ${deployMod} \
        --driver-memory ${driverMem} \
        --total-executor-cores ${totalCore} \
        --executor-cores ${perCore} \
        --executor-memory ${perMem} \
        --conf spark.driver.maxResultSize=20g \
        --conf spark.storage.memoryFraction=0.5 \
        --conf "spark.executor.extraJavaOptions=-XX:+UseG1GC" \
        --master ${master} \
        --class ${mainClass} ${jarURl} ${database} ${table} ${numPartitions} ${partitionType} \
#        --class ${mainClass} ${jarURl} ${database} ${table} ${partitionColumn} ${lowerBound} ${upperBound} ${numPartitions} \
#        --conf "spark.executor.extraJavaOptions=-XX:+UseG1GC -XX:+PrintFlagsFinal -XX:+PrintReferenceGC -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintAdaptiveSizePolicy -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark" \
#        --conf "spark.executor.extraJavaOptions=-XX:+UseG1GC -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+UnlockDiagnosticVMOptions" \
